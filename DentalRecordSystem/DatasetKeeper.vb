﻿Imports System.Data.OleDb
Imports DentalRecordSystem.DentalClinicDatabaseDataSetTableAdapters

Module DatasetKeeper

    Public Property Initiated As Boolean = False

    Public Property DCDataset As New DentalClinicDatabaseDataSet
    Public Property PRTable As New PatientInformationTableAdapter
    Public Property ACTable As New AccountsTableAdapter
    Public Property DTable As New DentistsTableAdapter
    Public Property TRRecord As New TreatmentRecordTableAdapter
    Public Property TableManager As New TableAdapterManager

    Public Sub init()
        Initiated = True
        PRTable.Fill(DCDataset.PatientInformation)
        ACTable.Fill(DCDataset.Accounts)
        DTable.Fill(DCDataset.Dentists)
        TRRecord.Fill(DCDataset.TreatmentRecord)
        TableManager.PatientInformationTableAdapter = PRTable
        TableManager.AccountsTableAdapter = ACTable
        TableManager.DentistsTableAdapter = DTable
        TableManager.TreatmentRecordTableAdapter = TRRecord
    End Sub
End Module
