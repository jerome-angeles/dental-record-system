﻿Public Class Login

    ''' <summary>
    ''' Clicking the username causes the username text to be gone
    ''' </summary>
    Private Sub TextBox1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox1.Click

        If Not String.IsNullOrWhiteSpace(TextBox1.Text) Then
            TextBox1.Text = ""
        End If
    End Sub

    ''' <summary>
    ''' Give the textbox the username text if there is nothing written to it
    ''' </summary>
    Private Sub TextBox1_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox1.LostFocus
        If String.IsNullOrWhiteSpace(TextBox1.Text) Then
            TextBox1.Text = "Username"
        End If
    End Sub

    Private Sub TextBox2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox2.GotFocus
        If TextBox2.Text = "Password" Then
            TextBox2.Text = ""
            TextBox2.UseSystemPasswordChar = True
            TextBox2.PasswordChar = "*"
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Accounts.Login(TextBox1.Text, TextBox2.Text) Then
            MainForm.Show()
            Me.Hide()
        Else
            MessageBox.Show("Login Failed", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub Login_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Upon load initialize the database
        If Initiated = False Then
            init()
        End If
    End Sub
End Class
