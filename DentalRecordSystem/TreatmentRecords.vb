﻿Public Class TreatmentRecords

    Private Sub TreatmentRecords_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.MdiParent = MainForm
        Me.WindowState = FormWindowState.Maximized
        Load()
    End Sub

    Private Sub Load()
        ClientList.Items.Clear()
        ClientList.Items.Add("<New Client>")
        For Each x As PatientRecords In PatientRecords.GetAll
            ClientList.Items.Add(x)
        Next
    End Sub

    Private Sub LoadWithPattern()
        ClientList.Items.Clear()
        ClientList.Items.Add("<New Client>")
        For Each x As PatientRecords In PatientRecords.GetAllWithPattern(TextBox1.Text)
            ClientList.Items.Add(x)
        Next
    End Sub

    Private Sub ClientList_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClientList.SelectedIndexChanged
        Try
            If ClientList.SelectedItem = "<New Client>" Then
                PatientRecord.Show()
                PatientRecord.action = "ToTreatmentRecord"
                Me.Close()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Load()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        LoadWithPattern()
    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.Click
        TextBox1.Text = ""
    End Sub

    Private Sub TextBox1_Enter(ByVal sender As System.Object, ByVal e As KeyPressEventArgs) Handles TextBox1.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Return) Then
            LoadWithPattern()
        Else
            e.Handled = False
        End If
    End Sub
End Class