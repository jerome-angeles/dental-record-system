﻿Imports System.Data.OleDb
Imports DentalRecordSystem.DentalClinicDatabaseDataSetTableAdapters

Public Class Accounts
    Implements IComparable
    Implements IDisposable

    Private Shared Property connection As New OleDbConnection(My.Settings.DentalClinicDatabaseConnectionString)

    Public Shared Property CurrentAccessLevel As AccessLevel

    Public Shared Function Login(ByVal username, ByVal password) As Boolean
        Dim command As New OleDbCommand("SELECT * FROM Accounts WHERE usrName = @username", connection)
        command.Parameters.Add("@username", OleDbType.VarChar).Value = username
        Dim realUsername, realPassword, AccessLevel As String
        Dim datareader As OleDbDataReader
        connection.Open()
        datareader = command.ExecuteReader
        While datareader.Read
            realUsername = datareader.GetString(0)
            realPassword = datareader.GetString(1)
            AccessLevel = datareader.GetString(2)
        End While
        connection.Close()
        Try
            If realUsername = username And password = realPassword Then
                If AccessLevel = "Administrator" Then
                    CurrentAccessLevel = DentalRecordSystem.AccessLevel.Administrator
                Else
                    CurrentAccessLevel = DentalRecordSystem.AccessLevel.NonAdministrator
                End If
                Return True
            End If
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function CompareTo(ByVal obj As Object) As Integer Implements System.IComparable.CompareTo
        Return 0
    End Function

#Region "IDisposable Support"
    Private disposedValue As Boolean ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: dispose managed state (managed objects).
            End If

            ' TODO: free unmanaged resources (unmanaged objects) and override Finalize() below.
            ' TODO: set large fields to null.
        End If
        Me.disposedValue = True
    End Sub

    ' TODO: override Finalize() only if Dispose(ByVal disposing As Boolean) above has code to free unmanaged resources.
    'Protected Overrides Sub Finalize()
    '    ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
    '    Dispose(False)
    '    MyBase.Finalize()
    'End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class

Public Enum AccessLevel
    Administrator
    NonAdministrator
End Enum