﻿Imports System.Data.OleDb
Imports DentalRecordSystem.DentalClinicDatabaseDataSetTableAdapters

Public Class PatientRecords
    Implements IComparable
    Implements IDisposable

#Region "Property Fields"
    Private Property ID As Integer
    Public Property LastName As String
    Public Property FirstName As String
    Public Property MiddleName As String
    Public Property Birthday As Date
    Public Property Nationality As String
    Public Property Age As Integer
    Public Property Sex As String
    Public Property Religion As String
    Public Property HomeAddress As String
    Public Property Occupation As String
    Public Property DentalInsurance As String
    Public Property EffectiveDate As Date
    Public Property ParentOrGuardianName As String
    Public Property ParentOrGuardianOccupation As String
    Public Property Refferer As String
    Public Property ReasonForConsultation As String
    Public Property Nickname As String
    Public Property HomeNumber As String
    Public Property OfficeNumber As String
    Public Property FaxNumber As String
    Public Property CellphoneNumber As String
    Public Property EmailAdd As String
    Public Property PreviousDentist As String
    Public Property LastDentalVisit As String
    Public Property TreatmentRecords As New ArrayList

#End Region

    Private Property method As String
    Private Shared Property connection As New OleDbConnection(My.Settings.DentalClinicDatabaseConnectionString)

    Public Sub New()
        method = "insert"
    End Sub

    Property ValidationErrorMessage As String

    Public Function GetID() As Integer
        Return ID
    End Function

    Public Sub New(ByVal ids As Integer)
        On Error Resume Next
        Dim command As New OleDbCommand("SELECT * FROM PatientInformation WHERE ID = @ids", connection)
        command.Parameters.AddWithValue("@ids", ids)
        Dim dataReader As OleDbDataReader
        connection.Open()
        dataReader = command.ExecuteReader()
        While dataReader.Read
            ID = dataReader.GetInt32(0)
            LastName = dataReader.GetString(1)
            FirstName = dataReader.GetString(2)
            MiddleName = dataReader.GetString(3)
            Birthday = dataReader.GetDateTime(4)
            Nationality = dataReader.GetString(5)
            Age = dataReader.GetInt32(6)
            Sex = dataReader.GetString(7)
            Religion = dataReader.GetString(8)
            HomeAddress = dataReader.GetString(9)
            Occupation = dataReader.GetString(10)
            DentalInsurance = dataReader.GetString(11)
            EffectiveDate = dataReader.GetDateTime(12)
            ParentOrGuardianName = dataReader.GetString(13)
            ParentOrGuardianOccupation = dataReader(14)
            Refferer = dataReader.GetString(15)
            ReasonForConsultation = dataReader.GetString(16)
            Nickname = dataReader.GetString(17)
            HomeNumber = dataReader.GetString(18)
            OfficeNumber = dataReader.GetString(19)
            FaxNumber = dataReader.GetString(20)
            CellphoneNumber = dataReader.GetString(21)
            EmailAdd = dataReader.GetString(22)
            PreviousDentist = dataReader.GetString(23)
            LastDentalVisit = dataReader.GetString(24)
        End While

        connection.Close()

        method = "update"

        'TODO: Add treatment RECORDS
    End Sub

    Public Sub Save()
        If method = "insert" Then
            Insert()
        Else
            Update()
        End If
    End Sub

    Private Sub Insert()
        'On Error Resume Next
        Dim command As New OleDbCommand("INSERT INTO `PatientInformation` (`LastName`, `FirstName`, `MiddleName`, `Birthday`, `Nationality`, `Age`, `Sex`, `Religion`, `HomeAddress`, `Occupation`, `Dentalnsurance`, `EffectiveDate`, `ParentOrGuardianName`, `ParentOrGuardianOccupation`, `Refferer`, `ReasonForConsultation`, `Nickname`, `HomeNumber`, `OfficeNumber`, `FaxNumber`, `CellphoneNumber`, `EmailAdd`, `PreviousDentist`, `LastDentalVisit`) VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, @10, @11, @12, @13, @14, @15, @16, @17, @18, @19, @20, @21, @22, @23)", connection)
        command.Parameters.Add("@0", OleDbType.VarChar).Value = LastName
        command.Parameters.Add("@1", OleDbType.VarChar).Value = FirstName
        command.Parameters.Add("@2", OleDbType.VarChar).Value = MiddleName
        command.Parameters.Add("@3", OleDbType.Date).Value = Birthday
        command.Parameters.Add("@4", OleDbType.VarChar).Value = Nationality
        command.Parameters.Add("@5", OleDbType.Integer).Value = Age
        command.Parameters.Add("@6", OleDbType.VarChar).Value = Sex
        command.Parameters.Add("@7", OleDbType.VarChar).Value = Religion
        command.Parameters.Add("@8", OleDbType.VarChar).Value = HomeAddress
        command.Parameters.Add("@9", OleDbType.VarChar).Value = Occupation
        command.Parameters.Add("@10", OleDbType.VarChar).Value = DentalInsurance
        command.Parameters.Add("@11", OleDbType.Date).Value = EffectiveDate
        command.Parameters.Add("@12", OleDbType.VarChar).Value = ParentOrGuardianName
        command.Parameters.Add("@13", OleDbType.VarChar).Value = ParentOrGuardianOccupation
        command.Parameters.Add("@14", OleDbType.VarChar).Value = Refferer
        command.Parameters.Add("@15", OleDbType.VarChar).Value = ReasonForConsultation
        command.Parameters.Add("@16", OleDbType.VarChar).Value = Nickname
        command.Parameters.Add("@17", OleDbType.VarChar).Value = HomeNumber
        command.Parameters.Add("@18", OleDbType.VarChar).Value = OfficeNumber
        command.Parameters.Add("@19", OleDbType.VarChar).Value = FaxNumber
        command.Parameters.Add("@20", OleDbType.VarChar).Value = CellphoneNumber
        command.Parameters.Add("@21", OleDbType.VarChar).Value = EmailAdd
        command.Parameters.Add("@22", OleDbType.VarChar).Value = PreviousDentist
        command.Parameters.Add("@23", OleDbType.VarChar).Value = LastDentalVisit
        connection.Open()
        command.ExecuteNonQuery()
        connection.Close()

        method = "update"
    End Sub

    Private Sub Update()
        'On Error Resume Next
        Dim command As New OleDbCommand("UPDATE PatientInformation SET LastName = @0, FirstName = @1, MiddleName = @2, Birthday = @3, Nationality = @4, Age = @5, Sex = @6, Religion = @7, HomeAddress = @8, Occupation = @9, Dentalnsurance = @10, EffectiveDate = @11, ParentOrGuardianName = @12, ParentOrGuardianOccupation = @13, Refferer = @14, ReasonForConsultation = @15, Nickname = @16, HomeNumber = @17, OfficeNumber = @18, FaxNumber = @19, CellphoneNumber = @20, EmailAdd = @21, PreviousDentist = @22, LastDentalVisit = @23 WHERE (ID = @ids)", connection)
        command.Parameters.Add("@0", OleDbType.VarChar).Value = LastName
        command.Parameters.Add("@1", OleDbType.VarChar).Value = FirstName
        command.Parameters.Add("@2", OleDbType.VarChar).Value = MiddleName
        command.Parameters.Add("@3", OleDbType.Date).Value = Birthday
        command.Parameters.Add("@4", OleDbType.VarChar).Value = Nationality
        command.Parameters.Add("@5", OleDbType.Integer).Value = Age
        command.Parameters.Add("@6", OleDbType.VarChar).Value = Sex
        command.Parameters.Add("@7", OleDbType.VarChar).Value = Religion
        command.Parameters.Add("@8", OleDbType.VarChar).Value = HomeAddress
        command.Parameters.Add("@9", OleDbType.VarChar).Value = Occupation
        command.Parameters.Add("@10", OleDbType.VarChar).Value = DentalInsurance
        command.Parameters.Add("@11", OleDbType.Date).Value = EffectiveDate
        command.Parameters.Add("@12", OleDbType.VarChar).Value = ParentOrGuardianName
        command.Parameters.Add("@13", OleDbType.VarChar).Value = ParentOrGuardianOccupation
        command.Parameters.Add("@14", OleDbType.VarChar).Value = Refferer
        command.Parameters.Add("@15", OleDbType.VarChar).Value = ReasonForConsultation
        command.Parameters.Add("@16", OleDbType.VarChar).Value = Nickname
        command.Parameters.Add("@17", OleDbType.VarChar).Value = HomeNumber
        command.Parameters.Add("@18", OleDbType.VarChar).Value = OfficeNumber
        command.Parameters.Add("@19", OleDbType.VarChar).Value = FaxNumber
        command.Parameters.Add("@20", OleDbType.VarChar).Value = CellphoneNumber
        command.Parameters.Add("@21", OleDbType.VarChar).Value = EmailAdd
        command.Parameters.Add("@22", OleDbType.VarChar).Value = PreviousDentist
        command.Parameters.Add("@23", OleDbType.VarChar).Value = LastDentalVisit
        command.Parameters.Add("@ids", OleDbType.Integer).Value = ID
        connection.Open()
        command.ExecuteNonQuery()
        connection.Close()
    End Sub

    Public Shared Function GetAll() As List(Of PatientRecords)
        Dim result As New List(Of PatientRecords)
        Dim command As New OleDbCommand("SELECT * FROM PatientInformation ORDER BY LastName ASC", connection)
        Dim dataReader As OleDbDataReader
        connection.Open()
        dataReader = command.ExecuteReader()
        Dim x As PatientRecords
        While dataReader.Read
            x = New PatientRecords
            x.ID = dataReader.GetInt32(0)
            x.LastName = dataReader.GetString(1)
            x.FirstName = dataReader.GetString(2)
            x.MiddleName = dataReader.GetString(3)
            x.Birthday = dataReader.GetDateTime(4)
            x.Nationality = dataReader.GetString(5)
            x.Age = dataReader.GetInt32(6)
            x.Sex = dataReader.GetString(7)
            x.Religion = dataReader.GetString(8)
            x.HomeAddress = dataReader.GetString(9)
            x.Occupation = dataReader.GetString(10)
            x.DentalInsurance = dataReader.GetString(11)
            x.EffectiveDate = dataReader.GetDateTime(12)
            x.ParentOrGuardianName = dataReader.GetString(13)
            x.ParentOrGuardianOccupation = dataReader(14)
            x.Refferer = dataReader.GetString(15)
            x.ReasonForConsultation = dataReader.GetString(16)
            x.Nickname = dataReader.GetString(17)
            x.HomeNumber = dataReader.GetString(18)
            x.OfficeNumber = dataReader.GetString(19)
            x.FaxNumber = dataReader.GetString(20)
            x.CellphoneNumber = dataReader.GetString(21)
            x.EmailAdd = dataReader.GetString(22)
            x.PreviousDentist = dataReader.GetString(23)
            x.LastDentalVisit = dataReader.GetString(24)
            x.method = "update"
            result.Add(x)
        End While
        connection.Close()
        Return result
        'TODO: Add treatment RECORDS
    End Function

    Public Shared Function GetAllWithPattern(ByVal pattern As String) As List(Of PatientRecords)
        Dim result As New List(Of PatientRecords)
        Dim command As New OleDbCommand("SELECT * FROM PatientInformation WHERE LastName & FirstName & MiddleName & NickName & ID & Sex LIKE " + "'%" + pattern + "%'" + "ORDER BY LastName ASC", connection)
        command.Parameters.Add("@pattern", OleDbType.VarChar).Value = pattern
        Dim dataReader As OleDbDataReader
        connection.Open()
        dataReader = command.ExecuteReader()
        Dim x As PatientRecords
        While dataReader.Read
            x = New PatientRecords
            x.ID = dataReader.GetInt32(0)
            x.LastName = dataReader.GetString(1)
            x.FirstName = dataReader.GetString(2)
            x.MiddleName = dataReader.GetString(3)
            x.Birthday = dataReader.GetDateTime(4)
            x.Nationality = dataReader.GetString(5)
            x.Age = dataReader.GetInt32(6)
            x.Sex = dataReader.GetString(7)
            x.Religion = dataReader.GetString(8)
            x.HomeAddress = dataReader.GetString(9)
            x.Occupation = dataReader.GetString(10)
            x.DentalInsurance = dataReader.GetString(11)
            x.EffectiveDate = dataReader.GetDateTime(12)
            x.ParentOrGuardianName = dataReader.GetString(13)
            x.ParentOrGuardianOccupation = dataReader(14)
            x.Refferer = dataReader.GetString(15)
            x.ReasonForConsultation = dataReader.GetString(16)
            x.Nickname = dataReader.GetString(17)
            x.HomeNumber = dataReader.GetString(18)
            x.OfficeNumber = dataReader.GetString(19)
            x.FaxNumber = dataReader.GetString(20)
            x.CellphoneNumber = dataReader.GetString(21)
            x.EmailAdd = dataReader.GetString(22)
            x.PreviousDentist = dataReader.GetString(23)
            x.LastDentalVisit = dataReader.GetString(24)
            x.method = "update"
            result.Add(x)
        End While
        connection.Close()
        Return result
        'TODO: Add treatment RECORDS
    End Function

    Public Function CompareTo(ByVal obj As Object) As Integer Implements System.IComparable.CompareTo
        Return 0
    End Function

    Public Overrides Function ToString() As String
        Return LastName + ", " + FirstName + " " + MiddleName + " '" + Nickname + "'"
    End Function

#Region "IDisposable Support"
    Private disposedValue As Boolean ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: dispose managed state (managed objects).
            End If

            ' TODO: free unmanaged resources (unmanaged objects) and override Finalize() below.
            ' TODO: set large fields to null.
        End If
        Me.disposedValue = True
    End Sub

    ' TODO: override Finalize() only if Dispose(ByVal disposing As Boolean) above has code to free unmanaged resources.
    'Protected Overrides Sub Finalize()
    '    ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
    '    Dispose(False)
    '    MyBase.Finalize()
    'End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

    Function Validate() As Boolean
        'Return Not String.IsNullOrWhiteSpace(FirstName) And String.IsNullOrWhiteSpace(LastName) And String.IsNullOrWhiteSpace(MiddleName)
        Return True
    End Function

End Class
