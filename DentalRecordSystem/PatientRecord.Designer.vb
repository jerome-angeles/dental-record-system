﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PatientRecord
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.LastNameField = New System.Windows.Forms.TextBox()
        Me.FirstNameField = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.MiddleNameField = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.GenderBox = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.BirthdayField = New System.Windows.Forms.DateTimePicker()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.NationiltyField = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.AgeField = New System.Windows.Forms.NumericUpDown()
        Me.ReligionField = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.HomeAddressField = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.OccupationField = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.DentalInsuranceField = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.EffectiveDateField = New System.Windows.Forms.DateTimePicker()
        Me.ParentOrGuardianOccupation = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.ParentOrGuardianName = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.ReffererField = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.EmailAddressField = New System.Windows.Forms.TextBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.CellphoneOrTelephoneNumberField = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.FaxNumberField = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.OfficeNumberField = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.HomeNumberField = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.NicknameField = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.ReasonForConsultationField = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.LastDentalVisitField = New System.Windows.Forms.TextBox()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.PreviousDentistField = New System.Windows.Forms.TextBox()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Label23 = New System.Windows.Forms.Label()
        CType(Me.AgeField, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Location = New System.Drawing.Point(9, 85)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(76, 15)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Last Name"
        '
        'LastNameField
        '
        Me.LastNameField.Location = New System.Drawing.Point(12, 59)
        Me.LastNameField.Name = "LastNameField"
        Me.LastNameField.Size = New System.Drawing.Size(267, 23)
        Me.LastNameField.TabIndex = 1
        '
        'FirstNameField
        '
        Me.FirstNameField.Location = New System.Drawing.Point(285, 59)
        Me.FirstNameField.Name = "FirstNameField"
        Me.FirstNameField.Size = New System.Drawing.Size(267, 23)
        Me.FirstNameField.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Location = New System.Drawing.Point(282, 85)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(78, 15)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "First Name"
        '
        'MiddleNameField
        '
        Me.MiddleNameField.Location = New System.Drawing.Point(558, 59)
        Me.MiddleNameField.Name = "MiddleNameField"
        Me.MiddleNameField.Size = New System.Drawing.Size(267, 23)
        Me.MiddleNameField.TabIndex = 5
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Location = New System.Drawing.Point(555, 85)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(91, 15)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Middle Name"
        '
        'GenderBox
        '
        Me.GenderBox.FormattingEnabled = True
        Me.GenderBox.Items.AddRange(New Object() {"Male", "Female"})
        Me.GenderBox.Location = New System.Drawing.Point(831, 59)
        Me.GenderBox.Name = "GenderBox"
        Me.GenderBox.Size = New System.Drawing.Size(129, 23)
        Me.GenderBox.TabIndex = 6
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Location = New System.Drawing.Point(828, 85)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(31, 15)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Sex"
        '
        'BirthdayField
        '
        Me.BirthdayField.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.BirthdayField.Location = New System.Drawing.Point(12, 107)
        Me.BirthdayField.Name = "BirthdayField"
        Me.BirthdayField.Size = New System.Drawing.Size(267, 23)
        Me.BirthdayField.TabIndex = 8
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Location = New System.Drawing.Point(9, 133)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(62, 15)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "BIrthday"
        '
        'NationiltyField
        '
        Me.NationiltyField.Location = New System.Drawing.Point(285, 107)
        Me.NationiltyField.Name = "NationiltyField"
        Me.NationiltyField.Size = New System.Drawing.Size(267, 23)
        Me.NationiltyField.TabIndex = 11
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Location = New System.Drawing.Point(282, 133)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(78, 15)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "Nationality"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.Location = New System.Drawing.Point(555, 129)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(32, 15)
        Me.Label7.TabIndex = 12
        Me.Label7.Text = "Age"
        '
        'AgeField
        '
        Me.AgeField.Location = New System.Drawing.Point(558, 109)
        Me.AgeField.Maximum = New Decimal(New Integer() {200, 0, 0, 0})
        Me.AgeField.Name = "AgeField"
        Me.AgeField.Size = New System.Drawing.Size(120, 23)
        Me.AgeField.TabIndex = 13
        '
        'ReligionField
        '
        Me.ReligionField.Location = New System.Drawing.Point(684, 109)
        Me.ReligionField.Name = "ReligionField"
        Me.ReligionField.Size = New System.Drawing.Size(276, 23)
        Me.ReligionField.TabIndex = 15
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.Transparent
        Me.Label8.Location = New System.Drawing.Point(681, 135)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(60, 15)
        Me.Label8.TabIndex = 14
        Me.Label8.Text = "Religion"
        '
        'HomeAddressField
        '
        Me.HomeAddressField.Location = New System.Drawing.Point(12, 153)
        Me.HomeAddressField.Name = "HomeAddressField"
        Me.HomeAddressField.Size = New System.Drawing.Size(666, 23)
        Me.HomeAddressField.TabIndex = 16
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.BackColor = System.Drawing.Color.Transparent
        Me.Label9.Location = New System.Drawing.Point(9, 179)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(101, 15)
        Me.Label9.TabIndex = 17
        Me.Label9.Text = "Home Address"
        '
        'OccupationField
        '
        Me.OccupationField.Location = New System.Drawing.Point(684, 153)
        Me.OccupationField.Name = "OccupationField"
        Me.OccupationField.Size = New System.Drawing.Size(276, 23)
        Me.OccupationField.TabIndex = 19
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.BackColor = System.Drawing.Color.Transparent
        Me.Label10.Location = New System.Drawing.Point(681, 179)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(82, 15)
        Me.Label10.TabIndex = 18
        Me.Label10.Text = "Occupation"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.BackColor = System.Drawing.Color.Transparent
        Me.Label11.Location = New System.Drawing.Point(9, 225)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(118, 15)
        Me.Label11.TabIndex = 21
        Me.Label11.Text = "Dental Insurance"
        '
        'DentalInsuranceField
        '
        Me.DentalInsuranceField.Location = New System.Drawing.Point(12, 199)
        Me.DentalInsuranceField.Name = "DentalInsuranceField"
        Me.DentalInsuranceField.Size = New System.Drawing.Size(267, 23)
        Me.DentalInsuranceField.TabIndex = 20
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.BackColor = System.Drawing.Color.Transparent
        Me.Label12.Location = New System.Drawing.Point(282, 225)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(98, 15)
        Me.Label12.TabIndex = 23
        Me.Label12.Text = "Effective Date"
        '
        'EffectiveDateField
        '
        Me.EffectiveDateField.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.EffectiveDateField.Location = New System.Drawing.Point(285, 199)
        Me.EffectiveDateField.Name = "EffectiveDateField"
        Me.EffectiveDateField.Size = New System.Drawing.Size(267, 23)
        Me.EffectiveDateField.TabIndex = 24
        '
        'ParentOrGuardianOccupation
        '
        Me.ParentOrGuardianOccupation.Location = New System.Drawing.Point(684, 247)
        Me.ParentOrGuardianOccupation.Name = "ParentOrGuardianOccupation"
        Me.ParentOrGuardianOccupation.Size = New System.Drawing.Size(276, 23)
        Me.ParentOrGuardianOccupation.TabIndex = 28
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.BackColor = System.Drawing.Color.Transparent
        Me.Label13.Location = New System.Drawing.Point(681, 273)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(82, 15)
        Me.Label13.TabIndex = 27
        Me.Label13.Text = "Occupation"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.BackColor = System.Drawing.Color.Transparent
        Me.Label14.Location = New System.Drawing.Point(9, 273)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(172, 15)
        Me.Label14.TabIndex = 26
        Me.Label14.Text = "Parent or Guardian Name"
        '
        'ParentOrGuardianName
        '
        Me.ParentOrGuardianName.Location = New System.Drawing.Point(12, 247)
        Me.ParentOrGuardianName.Name = "ParentOrGuardianName"
        Me.ParentOrGuardianName.Size = New System.Drawing.Size(666, 23)
        Me.ParentOrGuardianName.TabIndex = 25
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.BackColor = System.Drawing.Color.Transparent
        Me.Label15.Location = New System.Drawing.Point(9, 319)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(60, 15)
        Me.Label15.TabIndex = 30
        Me.Label15.Text = "Refferer"
        '
        'ReffererField
        '
        Me.ReffererField.Location = New System.Drawing.Point(12, 293)
        Me.ReffererField.Name = "ReffererField"
        Me.ReffererField.Size = New System.Drawing.Size(666, 23)
        Me.ReffererField.TabIndex = 29
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.EmailAddressField)
        Me.GroupBox1.Controls.Add(Me.Label22)
        Me.GroupBox1.Controls.Add(Me.CellphoneOrTelephoneNumberField)
        Me.GroupBox1.Controls.Add(Me.Label21)
        Me.GroupBox1.Controls.Add(Me.FaxNumberField)
        Me.GroupBox1.Controls.Add(Me.Label20)
        Me.GroupBox1.Controls.Add(Me.OfficeNumberField)
        Me.GroupBox1.Controls.Add(Me.Label19)
        Me.GroupBox1.Controls.Add(Me.HomeNumberField)
        Me.GroupBox1.Controls.Add(Me.Label18)
        Me.GroupBox1.Controls.Add(Me.NicknameField)
        Me.GroupBox1.Controls.Add(Me.Label17)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 346)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(656, 177)
        Me.GroupBox1.TabIndex = 31
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Contact Details"
        '
        'EmailAddressField
        '
        Me.EmailAddressField.Location = New System.Drawing.Point(221, 108)
        Me.EmailAddressField.Name = "EmailAddressField"
        Me.EmailAddressField.Size = New System.Drawing.Size(209, 23)
        Me.EmailAddressField.TabIndex = 13
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.BackColor = System.Drawing.Color.Transparent
        Me.Label22.Location = New System.Drawing.Point(218, 134)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(100, 15)
        Me.Label22.TabIndex = 12
        Me.Label22.Text = "Email Address"
        '
        'CellphoneOrTelephoneNumberField
        '
        Me.CellphoneOrTelephoneNumberField.Location = New System.Drawing.Point(6, 108)
        Me.CellphoneOrTelephoneNumberField.Name = "CellphoneOrTelephoneNumberField"
        Me.CellphoneOrTelephoneNumberField.Size = New System.Drawing.Size(209, 23)
        Me.CellphoneOrTelephoneNumberField.TabIndex = 11
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.BackColor = System.Drawing.Color.Transparent
        Me.Label21.Location = New System.Drawing.Point(3, 134)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(216, 15)
        Me.Label21.TabIndex = 10
        Me.Label21.Text = "Cellphone or Telephone Number"
        '
        'FaxNumberField
        '
        Me.FaxNumberField.Location = New System.Drawing.Point(436, 64)
        Me.FaxNumberField.Name = "FaxNumberField"
        Me.FaxNumberField.Size = New System.Drawing.Size(209, 23)
        Me.FaxNumberField.TabIndex = 9
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.BackColor = System.Drawing.Color.Transparent
        Me.Label20.Location = New System.Drawing.Point(433, 90)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(85, 15)
        Me.Label20.TabIndex = 8
        Me.Label20.Text = "Fax Number"
        '
        'OfficeNumberField
        '
        Me.OfficeNumberField.Location = New System.Drawing.Point(221, 64)
        Me.OfficeNumberField.Name = "OfficeNumberField"
        Me.OfficeNumberField.Size = New System.Drawing.Size(209, 23)
        Me.OfficeNumberField.TabIndex = 7
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.BackColor = System.Drawing.Color.Transparent
        Me.Label19.Location = New System.Drawing.Point(218, 90)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(100, 15)
        Me.Label19.TabIndex = 6
        Me.Label19.Text = "Office Number"
        '
        'HomeNumberField
        '
        Me.HomeNumberField.Location = New System.Drawing.Point(6, 64)
        Me.HomeNumberField.Name = "HomeNumberField"
        Me.HomeNumberField.Size = New System.Drawing.Size(209, 23)
        Me.HomeNumberField.TabIndex = 5
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.BackColor = System.Drawing.Color.Transparent
        Me.Label18.Location = New System.Drawing.Point(3, 90)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(100, 15)
        Me.Label18.TabIndex = 4
        Me.Label18.Text = "Home Number"
        '
        'NicknameField
        '
        Me.NicknameField.Location = New System.Drawing.Point(6, 20)
        Me.NicknameField.Name = "NicknameField"
        Me.NicknameField.Size = New System.Drawing.Size(267, 23)
        Me.NicknameField.TabIndex = 3
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.BackColor = System.Drawing.Color.Transparent
        Me.Label17.Location = New System.Drawing.Point(3, 46)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(72, 15)
        Me.Label17.TabIndex = 2
        Me.Label17.Text = "Nickname"
        '
        'ReasonForConsultationField
        '
        Me.ReasonForConsultationField.Location = New System.Drawing.Point(684, 293)
        Me.ReasonForConsultationField.Multiline = True
        Me.ReasonForConsultationField.Name = "ReasonForConsultationField"
        Me.ReasonForConsultationField.Size = New System.Drawing.Size(276, 107)
        Me.ReasonForConsultationField.TabIndex = 33
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.BackColor = System.Drawing.Color.Transparent
        Me.Label16.Location = New System.Drawing.Point(684, 403)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(162, 15)
        Me.Label16.TabIndex = 32
        Me.Label16.Text = "Reason for Consultation"
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox2.Controls.Add(Me.LastDentalVisitField)
        Me.GroupBox2.Controls.Add(Me.Label27)
        Me.GroupBox2.Controls.Add(Me.PreviousDentistField)
        Me.GroupBox2.Controls.Add(Me.Label28)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 529)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(656, 115)
        Me.GroupBox2.TabIndex = 32
        Me.GroupBox2.TabStop = False
        '
        'LastDentalVisitField
        '
        Me.LastDentalVisitField.Location = New System.Drawing.Point(6, 64)
        Me.LastDentalVisitField.Name = "LastDentalVisitField"
        Me.LastDentalVisitField.Size = New System.Drawing.Size(639, 23)
        Me.LastDentalVisitField.TabIndex = 5
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.BackColor = System.Drawing.Color.Transparent
        Me.Label27.Location = New System.Drawing.Point(3, 90)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(113, 15)
        Me.Label27.TabIndex = 4
        Me.Label27.Text = "Last Dental Visit"
        '
        'PreviousDentistField
        '
        Me.PreviousDentistField.Location = New System.Drawing.Point(6, 20)
        Me.PreviousDentistField.Name = "PreviousDentistField"
        Me.PreviousDentistField.Size = New System.Drawing.Size(639, 23)
        Me.PreviousDentistField.TabIndex = 3
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.BackColor = System.Drawing.Color.Transparent
        Me.Label28.Location = New System.Drawing.Point(3, 46)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(114, 15)
        Me.Label28.TabIndex = 2
        Me.Label28.Text = "Previous Dentist"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(888, 621)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 34
        Me.Button1.Text = "Cancel"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(807, 621)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 35
        Me.Button2.Text = "Save"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.BackColor = System.Drawing.Color.Transparent
        Me.Label23.Font = New System.Drawing.Font("Arial Rounded MT Bold", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(12, 23)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(248, 33)
        Me.Label23.TabIndex = 36
        Me.Label23.Text = "Add New Patient"
        '
        'PatientRecord
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.DentalRecordSystem.My.Resources.Resources.Dental_Background
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(983, 657)
        Me.Controls.Add(Me.Label23)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.ReasonForConsultationField)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.ReffererField)
        Me.Controls.Add(Me.ParentOrGuardianOccupation)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.ParentOrGuardianName)
        Me.Controls.Add(Me.EffectiveDateField)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.DentalInsuranceField)
        Me.Controls.Add(Me.OccupationField)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.HomeAddressField)
        Me.Controls.Add(Me.ReligionField)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.AgeField)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.NationiltyField)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.BirthdayField)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.GenderBox)
        Me.Controls.Add(Me.MiddleNameField)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.FirstNameField)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.LastNameField)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Arial Rounded MT Bold", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(999, 695)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(999, 695)
        Me.Name = "PatientRecord"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.Text = "New Patient"
        Me.TopMost = True
        CType(Me.AgeField, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents LastNameField As System.Windows.Forms.TextBox
    Friend WithEvents FirstNameField As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents MiddleNameField As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents GenderBox As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents BirthdayField As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents NationiltyField As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents AgeField As System.Windows.Forms.NumericUpDown
    Friend WithEvents ReligionField As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents HomeAddressField As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents OccupationField As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents DentalInsuranceField As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents EffectiveDateField As System.Windows.Forms.DateTimePicker
    Friend WithEvents ParentOrGuardianOccupation As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents ParentOrGuardianName As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents ReffererField As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents EmailAddressField As System.Windows.Forms.TextBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents CellphoneOrTelephoneNumberField As System.Windows.Forms.TextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents FaxNumberField As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents OfficeNumberField As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents HomeNumberField As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents NicknameField As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents ReasonForConsultationField As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents LastDentalVisitField As System.Windows.Forms.TextBox
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents PreviousDentistField As System.Windows.Forms.TextBox
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Label23 As System.Windows.Forms.Label
End Class
