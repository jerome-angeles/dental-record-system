﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.SystemToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SaveXMLToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SaveJSONToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SomeStuffHereToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AddClientToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EditClientToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeleteClientToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DentalRecordToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AddTreatmentRecordToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AboutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PrintReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PrintClientInformationToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LogoutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ImageList1
        '
        Me.ImageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit
        Me.ImageList1.ImageSize = New System.Drawing.Size(16, 16)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 728)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(1370, 22)
        Me.StatusStrip1.TabIndex = 0
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SystemToolStripMenuItem, Me.SomeStuffHereToolStripMenuItem, Me.DentalRecordToolStripMenuItem, Me.AboutToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(1370, 24)
        Me.MenuStrip1.TabIndex = 1
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'SystemToolStripMenuItem
        '
        Me.SystemToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SaveXMLToolStripMenuItem, Me.SaveJSONToolStripMenuItem, Me.LogoutToolStripMenuItem, Me.ExitToolStripMenuItem})
        Me.SystemToolStripMenuItem.Name = "SystemToolStripMenuItem"
        Me.SystemToolStripMenuItem.Size = New System.Drawing.Size(57, 20)
        Me.SystemToolStripMenuItem.Text = "System"
        '
        'SaveXMLToolStripMenuItem
        '
        Me.SaveXMLToolStripMenuItem.Name = "SaveXMLToolStripMenuItem"
        Me.SaveXMLToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.SaveXMLToolStripMenuItem.Text = "Save XML"
        '
        'SaveJSONToolStripMenuItem
        '
        Me.SaveJSONToolStripMenuItem.Name = "SaveJSONToolStripMenuItem"
        Me.SaveJSONToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.SaveJSONToolStripMenuItem.Text = "Save JSON"
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.ExitToolStripMenuItem.Text = "Exit"
        '
        'SomeStuffHereToolStripMenuItem
        '
        Me.SomeStuffHereToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AddClientToolStripMenuItem, Me.EditClientToolStripMenuItem, Me.DeleteClientToolStripMenuItem, Me.PrintClientInformationToolStripMenuItem})
        Me.SomeStuffHereToolStripMenuItem.Name = "SomeStuffHereToolStripMenuItem"
        Me.SomeStuffHereToolStripMenuItem.Size = New System.Drawing.Size(55, 20)
        Me.SomeStuffHereToolStripMenuItem.Text = "Clients"
        '
        'AddClientToolStripMenuItem
        '
        Me.AddClientToolStripMenuItem.Name = "AddClientToolStripMenuItem"
        Me.AddClientToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.AddClientToolStripMenuItem.Text = "Add Client"
        '
        'EditClientToolStripMenuItem
        '
        Me.EditClientToolStripMenuItem.Name = "EditClientToolStripMenuItem"
        Me.EditClientToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.EditClientToolStripMenuItem.Text = "Edit Client"
        '
        'DeleteClientToolStripMenuItem
        '
        Me.DeleteClientToolStripMenuItem.Name = "DeleteClientToolStripMenuItem"
        Me.DeleteClientToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.DeleteClientToolStripMenuItem.Text = "Delete Client"
        '
        'DentalRecordToolStripMenuItem
        '
        Me.DentalRecordToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AddTreatmentRecordToolStripMenuItem, Me.PrintReportToolStripMenuItem})
        Me.DentalRecordToolStripMenuItem.Name = "DentalRecordToolStripMenuItem"
        Me.DentalRecordToolStripMenuItem.Size = New System.Drawing.Size(93, 20)
        Me.DentalRecordToolStripMenuItem.Text = "Dental Record"
        '
        'AddTreatmentRecordToolStripMenuItem
        '
        Me.AddTreatmentRecordToolStripMenuItem.Name = "AddTreatmentRecordToolStripMenuItem"
        Me.AddTreatmentRecordToolStripMenuItem.Size = New System.Drawing.Size(170, 22)
        Me.AddTreatmentRecordToolStripMenuItem.Text = "Treament Records"
        '
        'AboutToolStripMenuItem
        '
        Me.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem"
        Me.AboutToolStripMenuItem.Size = New System.Drawing.Size(52, 20)
        Me.AboutToolStripMenuItem.Text = "About"
        '
        'PrintReportToolStripMenuItem
        '
        Me.PrintReportToolStripMenuItem.Name = "PrintReportToolStripMenuItem"
        Me.PrintReportToolStripMenuItem.Size = New System.Drawing.Size(170, 22)
        Me.PrintReportToolStripMenuItem.Text = "Print Report"
        '
        'PrintClientInformationToolStripMenuItem
        '
        Me.PrintClientInformationToolStripMenuItem.Name = "PrintClientInformationToolStripMenuItem"
        Me.PrintClientInformationToolStripMenuItem.Size = New System.Drawing.Size(199, 22)
        Me.PrintClientInformationToolStripMenuItem.Text = "Print Client Information"
        '
        'LogoutToolStripMenuItem
        '
        Me.LogoutToolStripMenuItem.Name = "LogoutToolStripMenuItem"
        Me.LogoutToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.LogoutToolStripMenuItem.Text = "Logout"
        '
        'MainForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(15.0!, 28.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(1370, 750)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Font = New System.Drawing.Font("Arial Rounded MT Bold", 18.0!)
        Me.ForeColor = System.Drawing.SystemColors.ControlText
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.IsMdiContainer = True
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Margin = New System.Windows.Forms.Padding(8, 6, 8, 6)
        Me.Name = "MainForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "MainForm"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents SomeStuffHereToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SystemToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SaveXMLToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SaveJSONToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AddClientToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EditClientToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DeleteClientToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DentalRecordToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AddTreatmentRecordToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AboutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PrintClientInformationToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PrintReportToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LogoutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
End Class
