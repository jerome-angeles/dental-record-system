﻿Public Class PatientRecord

    Public Property method = "insert"
    Public Property action = "none"

    Private Sub BirthdayField_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BirthdayField.ValueChanged
        On Error Resume Next
        AgeField.Value = BirthdayField.Value.Year - Today.Year
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim messageBoxResult = MessageBox.Show("Are you sure? (All fields will be deleted upon exit)", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)
        If messageBoxResult = Windows.Forms.DialogResult.Yes Then
            If action = "ToTreatmentRecord" Then
                TreatmentRecords.Show()
                Me.Close()
                Return
            End If
            Me.Close()
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        'On Error Resume Next

        Using x As New PatientRecords()
            x.LastName = LastNameField.Text
            x.FirstName = FirstNameField.Text
            x.MiddleName = MiddleNameField.Text
            x.Birthday = BirthdayField.Value
            x.Nationality = NationiltyField.Text
            x.Age = AgeField.Value
            x.Sex = GenderBox.SelectedItem.ToString
            x.Religion = ReligionField.Text
            x.HomeAddress = HomeAddressField.Text
            x.Occupation = OccupationField.Text
            x.DentalInsurance = DentalInsuranceField.Text
            x.EffectiveDate = EffectiveDateField.Value
            x.ParentOrGuardianName = ParentOrGuardianName.Text
            x.ParentOrGuardianOccupation = ParentOrGuardianOccupation.Text
            x.Refferer = ReffererField.Text
            x.ReasonForConsultation = ReasonForConsultationField.Text
            x.Nickname = NicknameField.Text
            x.HomeNumber = HomeNumberField.Text
            x.OfficeNumber = OfficeNumberField.Text
            x.FaxNumber = FaxNumberField.Text
            x.CellphoneNumber = CellphoneOrTelephoneNumberField.Text
            x.EmailAdd = EmailAddressField.Text
            x.PreviousDentist = PreviousDentistField.Text
            x.LastDentalVisit = LastDentalVisitField.Text
            If x.Validate() Then
                x.Save()
                If action = "ToTreatmentRecord" Then
                    TreatmentRecords.Show()
                    MessageBox.Show("Success!", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Me.Close()
                    Return
                End If
                Dim msgBoxResponse = MessageBox.Show("Success! Create Another?", "Message.", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                If msgBoxResponse = Windows.Forms.DialogResult.Yes Then
                    InitializeComponent()
                Else
                    Me.Close()
                End If
            Else
                MessageBox.Show("Error saving the record. " & x.ValidationErrorMessage, "Message", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
        End Using
    End Sub

    Private Sub PatientRecord_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.MdiParent = MainForm
    End Sub
End Class