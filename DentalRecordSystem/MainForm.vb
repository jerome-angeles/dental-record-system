﻿Public Class MainForm
    Private Sub AddClientToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AddClientToolStripMenuItem.Click
        PatientRecord.MdiParent = Me
        PatientRecord.Show()
    End Sub

    Private Sub AddTreatmentRecordToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AddTreatmentRecordToolStripMenuItem.Click
        TreatmentRecords.MdiParent = Me
        TreatmentRecords.Show()
    End Sub

    Private Sub ExitToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitToolStripMenuItem.Click
        Login.Close()
    End Sub

    Private Sub MainForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Accounts.CurrentAccessLevel = AccessLevel.NonAdministrator Then
            SaveXMLToolStripMenuItem.Enabled = False
            SaveJSONToolStripMenuItem.Enabled = False
        End If
    End Sub

    Private Sub LogoutToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LogoutToolStripMenuItem.Click
        Login.Show()
        Me.Close()
    End Sub
End Class