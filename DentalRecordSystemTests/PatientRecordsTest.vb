﻿Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DentalRecordSystem



'''<summary>
'''This is a test class for PatientRecordsTest and is intended
'''to contain all PatientRecordsTest Unit Tests
'''</summary>
<TestClass()> _
Public Class PatientRecordsTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''A test for PatientRecords Constructor
    '''</summary>
    <TestMethod()> _
    Public Sub PatientRecordsConstructorTest()
        DatasetKeeper_Accessor.init()
        Dim ids As Integer = 5 ' TODO: Initialize to an appropriate value

        Dim target As PatientRecords = New PatientRecords(ids)
        Assert.AreEqual(target.GetID, ids)
    End Sub

    '''<summary>
    '''A test for Insert
    '''</summary>
    <TestMethod(), _
     DeploymentItem("DentalRecordSystem.exe")> _
    Public Sub InsertTest()
        DatasetKeeper_Accessor.init()
        Dim target As PatientRecords_Accessor = New PatientRecords_Accessor() ' TODO: Initialize to an appropriate value
        target.LastName = "huehuehue"
        target.Insert()
        Assert.IsTrue(target.LastName = "huehuehue", "Target ID = " & target.ID)
    End Sub
End Class
